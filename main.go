package main

import (
	"log"

	notify "fs/notify"
	"github.com/fsnotify/fsnotify"
)

func main() {
	dirs, err := notify.DirsToWatch()
	if err != nil {
		log.Fatal(err)
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	defer watcher.Close()

	notify.SetupDirsWatch(watcher, dirs)
}
