package notify

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	dispatcher "fs/notify/dispatcher"
	"github.com/fsnotify/fsnotify"
)

// SetupDirsWatch is
func SetupDirsWatch(watcher *fsnotify.Watcher, dirs []string) {
	var ed = dispatcher.NewDefaultEventDispatcher()
	var writeEvent = dispatcher.NewEvent(fsnotify.Write.String())
	var createEvent = dispatcher.NewEvent(fsnotify.Create.String())
	var removeEvent = dispatcher.NewEvent(fsnotify.Create.String())

	ed.AddListener(writeEvent.Name, handleWriteEvent)
	ed.AddListener(createEvent.Name, handleWriteEvent)
	ed.AddListener(removeEvent.Name, handleWriteEvent)

	done := make(chan bool)

	go func() {
		for {
			select {
			case event := <-watcher.Events:
				//log.Println("event: ", event)

				var e = dispatcher.NewEvent(event.Op.String())
				ed.Dispatch(e.Name, e)

				if event.Op&fsnotify.Write == fsnotify.Write {
				}
			case err := <-watcher.Errors:
				log.Println("error: ", err)
			}
		}
	}()

	for _, d := range dirs {
		if err := watcher.Add(d); err != nil {
			log.Fatal(err)
		}
	}
	<-done
}

func handleWriteEvent(e dispatcher.Event) {
	log.Println("HANDLED BY A LISTENER!!! event: ", e.Name)
}

// DirsToWatch is
func DirsToWatch() (dirs []string, err error) {
	if len(os.Args) <= 1 {
		err = fmt.Errorf("Usage: %s <dir1> <dir2> ... <dirN>",
			filepath.Base(os.Args[0]))
		return dirs, err
	}
	for i := 1; i < len(os.Args); i++ {
		dirs = append(dirs, os.Args[i])
	}
	return dirs, nil
}
