package dispatcher

// Event is
type Event struct {
	Name string
}

// NewEvent is an Event constructor.
func NewEvent(name string) Event {
	return Event{
		Name: name,
	}
}

// EventDispatcher is a
type EventDispatcher interface {
	AddListener(eName string, listener func(e Event))
	RemoveListner(eName string, listener func(e Event))
	Dispatch(eName string, event Event)
}

// DefaultEventDispatcher is a
type DefaultEventDispatcher struct {
	listeners map[string][]func(event Event)
}

// NewDefaultEventDispatcher is
func NewDefaultEventDispatcher() *DefaultEventDispatcher {
	return &DefaultEventDispatcher{
		listeners: make(map[string][]func(event Event)),
	}
}

// AddListener is
func (ed *DefaultEventDispatcher) AddListener(eName string, listener func(e Event)) {
	ed.listeners[eName] = append(ed.listeners[eName], listener)
}

// RemoveListener is
func (ed *DefaultEventDispatcher) RemoveListener(eName string, listener func(e Event)) {
	if _, ok := ed.listeners[eName]; !ok {
		return
	}
	for i, l := range ed.listeners[eName] {
		if &l == &listener {
			ed.listeners[eName] = append(
				ed.listeners[eName][:i],
				ed.listeners[eName][i+1:]...,
			)
		}
	}
}

// Dispatch is
func (ed *DefaultEventDispatcher) Dispatch(eName string, e Event) {
	if _, ok := ed.listeners[eName]; !ok {
		return
	}

	for _, lsnr := range ed.listeners[eName] {
		lsnr(e)
	}
}
